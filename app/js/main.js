(function(doc){
    
    const dataset = 'dataset',
        clas = 'classList',
        activeClass = 'active',
        parentNode = 'parentNode',
        style = 'style',
        checked = 'checked',
        rangeSteps = {
            'min': 30,
            '8.33333333%': 32,
            '16.6666667%': 60,
            '25%': 64,
            '33.3333333%': 120,
            '41.6666666%': 128,
            '49.9999999%': 240,
            '58.3333332%': 256,
            '66.6666665%': 480,
            '74.9999998%': 512,
            '83.3333331%': 960,
            'max': 1024,
        };
    
    
    function getElem(name, wrap) {
        if (typeof wrap === 'string') {
            return doc.querySelector(wrap).querySelector(name);
        } else if (typeof wrap === 'object') {
            return wrap.querySelector(name);
        } else {
            return doc.querySelector(name);
        }
    }
    function getElems(name, wrap) {
        if (typeof wrap === 'string') {
            return getElem(wrap).querySelectorAll(name);
        } else if (typeof wrap === 'object') {
            return wrap.querySelectorAll(name);
        } else {
            return doc.querySelectorAll(name);
        }
    }

    function atr(elem, attrubute) {
        return elem.getAttribute(attrubute);
    }

    function match(element, className) {
        return element[clas].contains(className);
    }

    function getCoords(elem, name) {
        if (name) {
            var box = elem.getBoundingClientRect();
        } else {
            var box = getElem(elem).getBoundingClientRect();
        }

        return box.top + pageYOffset;

    }

    function bGColor(num, el) {
        let elem = getElem(el);
        elem[style].opacity = `${num}`;
    }

    function setBgp(yPos, el) {
        let elem = getElem(el);
        elem.style.backgroundPositionY = `${yPos}px`;
    }

    function transformTextWrap(yPos, el) {
        let elem = getElem(el);
        
        elem.style.transform = `translateY(-${yPos}px)`;
    }
    function transformTextWrapOptionTwo(yPos, el) {
        let elem = getElem(el);
        
        elem.style.transform = `translateY(${yPos}px)`;
    }

    function sitebarLinks(sectName) {
        for (let i = 0; i < sitebarLink.length; i++) {
            if (sitebarLink[i][dataset].sect == sectName) {
                sitebarLink[i][clas].add('active');
                if (i != 0) {
                    sitebar[clas].remove('white');
                } else {
                    sitebar[clas].add('white');
                }
            } else {
                sitebarLink[i][clas].remove('active');
            }
        };
    }

    function smoothScroll(target) {
        if (target.hash !== '') {
            const hash = target.hash.replace("#", "")
            const link = doc.getElementsByName(hash);
            //Find the where you want to scroll
            const position = getCoords(link[0], true)
            let top = window.scrollY,
                num = 35;
            
            if (window.innerWidth < 768) {
                num = 75
            }

            let smooth = setInterval(() => {
                let leftover = position - top,
                    topOver = top - position;
                // console.log();

                if (top === position) {
                    clearInterval(smooth)
                }

                else if (position > top && leftover < num && position !== 0) {
                    top += leftover
                    window.scrollTo(0, top)
                }

                else if (position > (top - num) && position !== 0 && leftover > 0) {
                    top += num
                    window.scrollTo(0, top)
                }

                else if (position < top && topOver > num) {
                    top -= num;
                    
                    if (topOver < num) {
                        // top -= topOver;
                        window.scrollTo(0, position)
                    } else {
                        window.scrollTo(0, top)
                    }
                }
            }, 6)
        }
    }

    function unique(arr) {
        let result = [];

        for (let str of arr) {
            if (!result.includes(str)) {
                result.push(str);
            }
        }

        return result;
    }

    function hoverMatchesElements(id) {
        for (let i = 0; i < matchItemTitle.length; i++) {
            if (matchItemTitle[i][dataset].id == id) {
                matchItemTitle[i][clas].add('hover')
            } else {
                matchItemTitle[i][clas].remove('hover')
            }
        }
        for (let i = 0; i < matchItemData.length; i++) {
            if (matchItemData[i][dataset].id == id) {
                matchItemData[i][clas].add('hover')
            } else {
                matchItemData[i][clas].remove('hover')
            }
        }
    }
    getElem('.prel')[clas].add('show');
    setTimeout(() => {
        getElem('.prel')[clas].add('hide');
        if (getElem('.chs') && window.innerWidth > 1000){
            getElem('.chs')[clas].add('hi')
        } else if (getElem('.chs') && window.innerWidth < 1000) {
            getElem('.chs')[clas].add('hi')
        }
    }, 1050);

    const searchInput = getElem('input[data-search]'),
          searchBlock = getElem('.search'),
          navigation = getElem('.nav'),
          burgerBtn = getElem('.btn-menu'),
          sitebar = getElem('.stb'),
          sitebarLink = getElems('.stb-lnk'),
          machingInputs = getElems('input[type="checkbox"]'),
          modal = getElem('.modal'),
          modCls = getElem('.m-cls'),
          btnPreview = getElems('.img-prvw'),
          bmaches = getElems('.bt-mach'),
          contactForm = getElem('.modal-form'),
          submitBtn = getElem('.thnks'),
          modalForm = getElem('.mod-form'),
          matchItemTitle = getElems('.titl-itm'),
          matchItemData = getElems('.data-itm'),
          sliderItem = getElems('.itm-sl'),
          matchBlock = getElem('.match'),
          cardItems = getElems('.pr-c'),
          filterBtn = getElems('.flt-btn'),
          filterSataBtn = getElems('.sata-btn'),
          range = getElem('.inp-range'),
          tooltip = getElem('.tltp'),
          sataCards = getElems('div[data-sect-name="sata"]'),
          resetFilterSata = getElem('.cls-btn'),
          searchForm = getElem('.nav__search-form'),
          insideSitebarLink = getElems('.ins-link');
          
    let matchingItems = [],
        matchPostTypes = [],
        btnTop,
        imgProdTop,
        rangeSlider,
        filterSectName = null,
        filterSectDdr = null,
        filterSectGb = null,
        filterSataX = null,
        filterRangeVal = null,
        imgProdWrapFixed = null,
        modWrapProduct = getElem('.mod-wrap'),
        machHoverTitleWrap = null,
        matchHoverDataWrap = null;
    
    if (bmaches[0]) {
        btnTop = getCoords(bmaches[0], 'name');
    }
    if (insideSitebarLink) {
        let page = window.location.pathname.replace(/[^A-Za-z]/g, '');
        for (let link of insideSitebarLink) {
            if(link.name == page) {
                link[clas].add('active');
            } 
        }
    }
    // searchForm.addEventListener('submit', (e) => {
    //     e.preventDefault();
    //     const data = {
    //         action: 'search_products',
    //         search_query: searchInput.value,
    //         method: 'post'
    //     }
    //     $.ajax({
    //         url: window.wp.ajax_url,
    //         data,
    //         type: 'POST',
    //         success(res) {
    //             modal[clas].add('open');
    //             doc.body[style].overflow = 'hidden';
    //             $('.open').empty();
    //             $('.open').html(res);
    //             searchInput.value = '';
    //         },
    //         error(error) {
    //             searchInput[clas].add('error');
    //             setTimeout(() => searchInput[clas].remove('error'), 3000)
    //         }
    //     });
        
    // });
    searchInput.addEventListener('focus', () => searchBlock[clas].add(activeClass));
    searchInput.addEventListener('blur', () => searchBlock[clas].remove(activeClass));
    
    doc.addEventListener('click', function(e){
        let target = e.target;

        // while(target != this) {

            if(match(target, 'btn-menu')) {
                navigation[clas].toggle('menu-open');
                burgerBtn[clas].toggle('active');
                return;
            }

            if(match(target, 'inp-ch')) {
                let prevEl = target,
                    arr = [];

                for (let i = 0; i < machingInputs.length; i++) {
                    if (machingInputs[i][checked]) {
                        arr.push(machingInputs[i][dataset].id);
                        matchPostTypes.push(machingInputs[i][dataset].type);
                    } else {
                        for(let itm = 0; itm < arr.length; itm++) {
                            if(arr[itm].includes(machingInputs[i][dataset].id)){
                                arr.splice(itm, 1)
                            }
                        }
                    }
                }
                matchingItems = arr;
                if (matchingItems.length > 1) {
                  bmaches[0][clas].add("active");
                } else {
                  bmaches[0][clas].remove("active");
                }
                return;
            }

            if (match(target, 'stb-lnk') || match(target, 'ink-go')) {
                e.preventDefault();
                smoothScroll(target);
                return;
            }

            if (match(target, 'pr-c') && !match(target, 'inp-ch')) {
                // let action = getElem('.cps')[dataset].actionProduct;
                // var data = {
                //     action,
                //     id: target[dataset].postid,
                //     post: target[dataset].posttype,
                //     method: 'post',
                // };
                // $.post(window.wp.ajax_url, data, (res) => {
                //     $('.open').empty();
                //     $('.open').html(res);
                    imgProdWrapFixed = getElem('.wimg-prod'),
                    imgProdTop = getCoords(imgProdWrapFixed, 'name');
                // })
                modal[clas].add('open');
                doc.body[style].overflow = 'hidden';
                return;
            }

            if (match(target, 'm-cls')) {
                if(target[dataset].cont) {
                    contactForm[clas].remove('open');
                    doc.body[style].overflow = 'visible';
                }
                if (target[dataset].match) {
                    matchBlock[clas].remove('open');
                    doc.body[style].overflow = 'visible';
                }
                modal[clas].remove('open');
                doc.body[style].overflow = 'visible';
                return;
            }

            if (match(target, 'w-pr')) {
                window.print();
                return;
            }

            if(match(target, 'img-prvw')) {
                let id = target[dataset].id;
                const imgView = getElems('.img-vw');
                for (let i = 0; i < imgView.length; i++) {
                    if (imgView[i][dataset].id == id) {
                        imgView[i][clas].add('active');
                        btnPreview[i][clas].add('active');
                    } else {
                        imgView[i][clas].remove('active');
                        btnPreview[i][clas].remove('active');
                    }
                }

                return;
            }
                
            if(match(target, 'contact') || match(target, 'foot-cont')) {
                e.preventDefault();
                navigation[clas].remove('menu-open');
                burgerBtn[clas].remove('active');
                contactForm[clas].add('open');
                doc.body[style].overflow = 'hidden';
            }

            // if(match(target, 'submit') || match(target, 'send')) {
            //     e.preventDefault();
            //     if(match(target, 'send')) {
            //         contactForm[clas].add('open');
            //     }
            //     modalForm[clas].add('thnks');
            //     setTimeout(() => {
            //         contactForm[clas].remove('open');
            //         modalForm[clas].remove('thnks');
            //         doc.body[style].overflow = 'visible';
            //     }, 2000);
            // }

            if (match(target, 'bt-mach')) {
                // let action = getElem('.cps')[dataset].actionMatch;
                
                // let data = {
                //     action,
                //     postId: matchingItems,
                //     postType: unique(matchPostTypes),
                //     method: 'post'
                // }
                // $.post(window.wp.ajax_url, data, res => {
                //     $(matchBlock).html(res)
                //     machHoverTitleWrap = getElem('.hov-titl');
                //     matchHoverDataWrap = getElem('.hov-data');
                    const matchSlider = new Swiper('.slider-match', {
                        slidesPerView: 'auto',
                        spaceBetween: 0,
                        scrollbar: {
                            el: (sliderItem.length > 3) ? '.swiper-scrollbar' : '',
                            draggable: true,
                        },
                    });
                // });
                matchBlock[clas].add('open');
                doc.body[style].overflow = 'hidden';
            }

            if (match(target, 'flt-btn')) {
                if (target[dataset].sectName != filterSectName) {
                    filterSectName = target[dataset].sectName;
                }
                if (target[dataset].gb != 'null' && target[dataset].gb != filterSectGb) {
                    filterSectGb = target[dataset].gb;
                }
                if (target[dataset].ddr != 'null' && target[dataset].ddr != filterSectDdr) {
                    filterSectDdr = target[dataset].ddr;
                }
                if (match(target, 'active')) {
                    filterSectName = null;
                    filterSectGb = null;
                    filterSectDdr = null;
                }

                
                for (let b = 0; b < filterBtn.length; b++) {
                    if (filterBtn[b][dataset].sectName == filterSectName) {
                        if (filterBtn[b][dataset].ddr === filterSectDdr || filterBtn[b][dataset].gb === filterSectGb) {

                            filterBtn[b][clas].add('active');

                        } else {
                            filterBtn[b][clas].remove('active');
                        }
                    } else {
                        filterBtn[b][clas].remove('active');
                    }
                }

                for (let i = 0; i < cardItems.length; i++) {
                    let cardName = cardItems[i][dataset].sectName,
                        cardDataGb = cardItems[i][dataset].gb || null,
                        cardDataDdr = cardItems[i][dataset].ddr || null;
                        if (filterSectName === cardName) {
                            cardItems[i][clas].add('hide');
                            if (filterSectGb != null && filterSectDdr != null) {
                                if (cardDataGb == filterSectGb && cardDataDdr === filterSectDdr) {
                                    cardItems[i][clas].remove('hide');
                                }
                            } else {
                                if (cardDataDdr === filterSectDdr) {
                                    cardItems[i][clas].remove('hide');
                                }
                                if (cardDataGb == filterSectGb) {
                                    cardItems[i][clas].remove('hide');
                                }
                            }
                            
                        
                        } else {
                            cardItems[i][clas].remove('hide');
                        }
                }
            }

            if(match(target, 'sata-btn')) {
                resetFilterSata[clas].add(activeClass);
                if (target[dataset].x != filterSataX) {
                    filterSataX = target[dataset].x;
                }
                if(match(target, activeClass)) {
                    filterSataX = null;
                    filterRangeVal = null;
                    rangeSlider.set(30);
                    resetFilterSata[clas].remove(activeClass);
                }

                for (let i = 0; i < filterSataBtn.length; i++) {
                    if (filterSataBtn[i][dataset].x == filterSataX) {
                        filterSataBtn[i][clas].add(activeClass);
                    } else {
                        filterSataBtn[i][clas].remove(activeClass);
                    }
                }
                for (let i = 0; i < sataCards.length; i++) {
                    let cardDataX = sataCards[i][dataset].ddr,
                        cardDataGb = sataCards[i][dataset].gb;
                    if (!(cardDataX == filterSataX)) { 
                        sataCards[i][clas].add('hide');
                    } else {
                        sataCards[i][clas].remove('hide');
                    }
                    if (filterRangeVal != null) {
                        if (cardDataX == filterSataX && cardDataGb == filterRangeVal) {
                            sataCards[i][clas].remove('hide');
                        } else {
                            sataCards[i][clas].add('hide');
                        }
                    }
                    if (filterSataX == null) {
                        sataCards[i][clas].remove('hide');
                    }
                }
                // console.log(filterSataX, 'filterSataX')
            }

            if (match(target, 'cls-btn')) {
                filterSataX = null;
                filterRangeVal = null;
                rangeSlider.set(30);
                resetFilterSata[clas].remove(activeClass);

                for (let i = 0; i < filterSataBtn.length; i++) {
                    filterSataBtn[i][clas].remove(activeClass);
                }
                for (let i = 0; i < sataCards.length; i++) {
                    sataCards[i][clas].remove('hide');
                }
            }
            
            if(match(target, 'w-pr')) {
                window.print();
            }
    });

    doc.addEventListener('scroll', function (e) {
        
        let yScrollPosition = window.scrollY;
        // console.log(yScrollPosition)
        
        if (getElem('.hhp') && window.innerWidth >= 768) {
            let homecase = getElem('.hhp')[dataset].sect,
                power = getElem('.hps')[dataset].sect,
                memory = getElem('.hms-sect')[dataset].sect,
                ssd = getElem('.hss')[dataset].sect;
            

            


            // if (bodyData == 'one') {
            //     // if (yScrollPosition > (getCoords('.hps') - 600)) {
            //     //     transformTextWrap(((yScrollPosition * 2) / 3), '.hw');
            //     //     // bGColor(9, '.hhp .bg')
            //     // } else {
            //     //     transformTextWrap(0, '.hw');
            //     //     // bGColor(0, '.hhp .bg')
            //     // }
                
            //     // if (yScrollPosition > (getCoords('.hms') - 600)) {
            //     //     transformTextWrap(((yScrollPosition * 2) / 7), '.hpw');
            //     //     // bGColor(9, '.hps .bg')
            //     // } else {
            //     //     transformTextWrap(0, '.hpw');
            //     //     // bGColor(0, '.hps .bg')
            //     // }
                
            //     // if (yScrollPosition > (getCoords('.hss') - 600)) {
            //     //     transformTextWrap(((yScrollPosition * 2) / 10), '.hmw');
            //     //     // bGColor(9, '.hms .bg')
            //     // } else {
            //     //     transformTextWrap(0, '.hmw');
            //     //     // bGColor(0, '.hms .bg')
            //     // } 
            //     setBgp(((yScrollPosition + 800) * 2) / 30, '.hhp');
            //     setBgp((((yScrollPosition - 100) * 2) / 30), '.hps');
            //     setBgp(((yScrollPosition - 300) * 2) / 30, '.hms');
            //     setBgp((((yScrollPosition - 300) * 2) / 30), '.hss');

            //     if (yScrollPosition < getCoords('.hps')) {
            //         sitebarLinks(homecase);
            //     }
            //     if (yScrollPosition >= (getCoords('.hps') - 300) && yScrollPosition < getCoords('.hms')) {
            //         sitebarLinks(power)
            //     }
            //     if (yScrollPosition >= (getCoords('.hms') - 300) && yScrollPosition < getCoords('.hss')) {
            //         sitebarLinks(memory)
            //     }
            //     if (yScrollPosition >= (getCoords('.hss') - 300)) {
            //         sitebarLinks(ssd)
            //     }
            // }

            // if (bodyData == 'two') {
            //     if (yScrollPosition > (getCoords('.hps') - 600)) {
            //         transformTextWrap(((yScrollPosition * 2) / 3), '.hw');
            //         // bGColor(9, '.hhp .bg')
            //     } else {
            //         transformTextWrap(0, '.hw');
            //         // bGColor(0, '.hhp .bg')
            //     }

            //     if (yScrollPosition > (getCoords('.hms') - 500)) {
            //         transformTextWrapOptionTwo(((yScrollPosition * 2) / 7), '.hpw');
            //         // bGColor(9, '.hps .bg')
            //     } else {
            //         transformTextWrapOptionTwo(0, '.hpw');
            //         // bGColor(0, '.hps .bg')
            //     }

            //     if (yScrollPosition > (getCoords('.hss') - 500)) {
            //         transformTextWrap(((yScrollPosition * 2) / 10), '.hmw');
            //         // setBgp(((yScrollPosition - 300) * 2) / 100, '.hms');
            //         // bGColor(9, '.hms .bg')
            //     } else {
            //         transformTextWrap(0, '.hmw');
            //         // bGColor(0, '.hms .bg')
            //     }
            //     setBgp(((yScrollPosition + 800) * 2) / 50, '.hhp');
            //     setBgp(-(((yScrollPosition - 100) * 2) / 20), '.hps');
            //     setBgp((-300 + (((yScrollPosition - 300) * 2) / 50)), '.hms');
            //     setBgp(170 + (-(((yScrollPosition - 300) * 2) / 20)), '.hss');

            //     if (yScrollPosition < getCoords('.hps')) {
            //         sitebarLinks(homecase);
            //     }
            //     if (yScrollPosition >= (getCoords('.hps') - 300) && yScrollPosition < getCoords('.hms')) {
            //         sitebarLinks(power)
            //     }
            //     if (yScrollPosition >= (getCoords('.hms') - 300) && yScrollPosition < getCoords('.hss')) {
            //         sitebarLinks(memory)
            //     }
            //     if (yScrollPosition >= (getCoords('.hss') - 300)) {
            //         sitebarLinks(ssd)
            //     }
            // }

            // if (bodyData == 'three') {
            //     let sect1 = (getCoords('.hps') - (getElem('.hhp').offsetHeight - 300)),
            //         sect2 = (getCoords('.hps') - (getElem('.hhp').offsetHeight - 300)),
            //         sect3 = (getCoords('.hms') - (getElem('.hps').offsetHeight - 300)),
            //         sect4 = (getCoords('.hss') - (getElem('.hms').offsetHeight - 300));

            //     if (yScrollPosition > sect1) {
            //         getElem('.hhp')[clas].remove('visible');
            //     } else {
            //         getElem('.hhp')[clas].add('visible');
            //     }

            //     if (yScrollPosition >= sect2) {
            //         getElem('.hps')[clas].add('visible');
            //     } else {
            //         getElem('.hps')[clas].remove('visible');
            //     }

            //     if (yScrollPosition >= sect3) {
            //         getElem('.hps')[clas].remove('visible');
            //         getElem('.hms')[clas].add('visible');
            //     } else {
            //         getElem('.hms')[clas].remove('visible');
            //     }

            //     if (yScrollPosition >= sect4) {
            //         getElem('.hms')[clas].remove('visible');
            //         getElem('.hss')[clas].add('visible');
            //     } else {
            //         getElem('.hss')[clas].remove('visible');
            //     }

            //     if (yScrollPosition > (getCoords('.hps') - 400)) {
            //         bGColor(0, '.hhp')
            //     } else {
            //         bGColor(1, '.hhp')
            //     }

            //     if (yScrollPosition > (getCoords('.hms') - 400)) {
            //         bGColor(0, '.hps')
            //     } else {
            //         bGColor(1, '.hps')
            //     }

            //     if (yScrollPosition > (getCoords('.hss') - 400)) {
            //         bGColor(0, '.hms')
            //     } else {
            //         bGColor(1, '.hms')
            //     }

            //     if (yScrollPosition < getCoords('.hps')) {
            //         sitebarLinks(homecase);
            //     }
            //     if (yScrollPosition >= (getCoords('.hps') - 300) && yScrollPosition < getCoords('.hms')) {
            //         sitebarLinks(power)
            //     }
            //     if (yScrollPosition >= (getCoords('.hms') - 300) && yScrollPosition < getCoords('.hss')) {
            //         sitebarLinks(memory)
            //     }
            //     if (yScrollPosition >= (getCoords('.hss') - 300)) {
            //         sitebarLinks(ssd)
            //     }
                
            // }
            
            
            

            if (getCoords(sitebarLink[3], true) > getCoords('.hps')) {
                sitebar[clas].add('black-3');
            }
            if (getCoords(sitebarLink[2], true) > getCoords('.hps')) {
                sitebar[clas].add('black-2');
            }
            if (getCoords(sitebarLink[1], true) > getCoords('.hps')) {
                sitebar[clas].add('black-1');
            }
            if (getCoords(sitebarLink[0], true) > getCoords('.hps')) {
                sitebar[clas].add('black-0');
            }
            
            if (getCoords(sitebarLink[0], true) < getCoords('.hps')) {
                sitebar[clas].remove('black-0');
            }
            if (getCoords(sitebarLink[1], true) < getCoords('.hps')) {
                sitebar[clas].remove('black-1');
                
            }
            if (getCoords(sitebarLink[2], true) < getCoords('.hps')) {
                sitebar[clas].remove('black-2');
                
            }
            if (getCoords(sitebarLink[3], true) < getCoords('.hps')) {
                sitebar[clas].remove('black-3');
            }

            
            
            
            
            if (yScrollPosition > getCoords('.hps')) {

                getElem('.hms')[clas].add('fix');
            } else {
                getElem('.hms')[clas].remove('fix');
            }
            if (yScrollPosition < getCoords('.hps')) {
                sitebarLinks(homecase);
            }
            if (yScrollPosition >= (getCoords('.hps') - 300) && yScrollPosition < getCoords('.hms-sect')) {
                sitebarLinks(power)
            }
            if (yScrollPosition >= (getCoords('.hms-sect') - 300) && yScrollPosition < getCoords('.hss')) {
                // console.log('kol')
                sitebarLinks(memory)
            }
            if (yScrollPosition >= (getCoords('.hss') - 300)) {
                sitebarLinks(ssd)
            }
            // if (yScrollPosition >= (getCoords('.hms-sect') - 300) && yScrollPosition < getCoords('.hss')) {
            //     sitebarLinks(memory)
            // }

            if (yScrollPosition > (getCoords('.hps') - 600)) {
                transformTextWrap(((yScrollPosition * 2) / 3), '.hw');

                // bGColor(9, '.hhp .bg')
            } else {
                transformTextWrap(0, '.hw');
                // bGColor(0, '.hhp .bg')
            }

            if (yScrollPosition > (getCoords('.hms-sect') - 600)) {
                transformTextWrap(((yScrollPosition * 2) / 7), '.hpw');
                // bGColor(9, '.hps .bg')
            } else {
                transformTextWrap(0, '.hpw');
                // bGColor(0, '.hps .bg')
            }

            if (yScrollPosition > (getCoords('.hss') - 600)) {
                transformTextWrap(((yScrollPosition * 2) / 60), '.hmw');
                // bGColor(9, '.hms .bg')
            } else {
                transformTextWrap(0, '.hmw');
                // bGColor(0, '.hms .bg')
            }
            
            
        }

        if (getElem('.chs')) {
            if (yScrollPosition > (getCoords('.cps') - 1400) && window.innerWidth > 768) {
                transformTextWrap(((yScrollPosition * 2) / 7), '.chh');
                getElem('.chs')[clas].add('z');
            } else {
                getElem('.chs')[clas].remove('z');
                transformTextWrap(0, '.chh');
            }
            if (yScrollPosition > (getCoords('.cps') - 1400) && window.innerWidth > 768) {
                getElem('.chs')[clas].add('z');
            } else {
                getElem('.chs')[clas].remove('z');
            }
            // if (yScrollPosition > 0) {
            //     setBgp(((yScrollPosition + 300) * 2) / 25, '.chs');
            // }
        }

        if (bmaches) {
            if (yScrollPosition >= btnTop) {
                bmaches[0][clas].add('fixed')
            } else if (yScrollPosition < btnTop) {
                bmaches[0][clas].remove('fixed')
            }
        }

        

        return;
    });

    if (range) {
        rangeSlider = noUiSlider.create(range, {
            start: 30,
            snap: true,
            format: {
                to: function (value) {
                    return Math.floor(value);
                },
                from: function (value) {
                    return Math.floor(value);
                }
            },
            tooltips: true,
            behaviour: 'snap',
            // connect: [true, false],
            range: rangeSteps
        });
        rangeSlider.on('change', function (value) {
            filterRangeVal = value.toString();
            resetFilterSata[clas].add(activeClass);
            for (let i = 0; i < sataCards.length; i++) {
                let cardDataX = sataCards[i][dataset].ddr,
                    cardDataGb = sataCards[i][dataset].gb;

                sataCards[i][clas].add('hide');

                if (cardDataGb == filterRangeVal && cardDataX == filterSataX) {
                    sataCards[i][clas].remove('hide');
                }
                if (filterSataX == null) {
                    if (cardDataGb == filterRangeVal) {
                        sataCards[i][clas].remove('hide');
                    }
                }
            }
        });
    }

    if (modWrapProduct && window.innerWidth > 768) {
        modWrapProduct.addEventListener('scroll', function(e) {
    
            let yScrollPosition = e.target.scrollTop;
            imgProdWrapFixed[clas].add('fixed');
            imgProdWrapFixed[style].top = `${yScrollPosition + 50}px`;

        });
    }

    function matchHoverEl(e) {
        let target = e.target;
        
        if (match(target, 'titl-itm')) {
            let idTitle = target[dataset].id;

            hoverMatchesElements(idTitle);
        }
        if (match(target, 'data-itm')) {
            let idData = target[dataset].id;

            hoverMatchesElements(idData); 
        }
    }

    if (machHoverTitleWrap) {
        machHoverTitleWrap.addEventListener('mousemove', matchHoverEl); 
        matchHoverDataWrap.addEventListener('mousemove', matchHoverEl); 
    }

    svg4everybody({});

    const dimm = {
        CARDS: getElems('.pr-c[data-sect-name="dimm"]'),
        ALL_FILTERS: getElems('.flt-btn[data-sect-name="dimm"]'),
        DDR_FILTER: getElems('.flt-ddr[data-sect-name="dimm"]'),
        GB_FILTER: getElems('.flt-gb[data-sect-name="dimm"]')
    };
    const sodimm = {
        CARDS: getElems('.pr-c[data-sect-name="sodimm"]'),
        ALL_FILTERS: getElems('.flt-btn[data-sect-name="sodimm"]'),
        DDR_FILTER: getElems('.flt-ddr[data-sect-name="sodimm"]'),
        GB_FILTER: getElems('.flt-gb[data-sect-name="sodimm"]')
    };
    const dom = {
        CARDS: getElems('.pr-c[data-sect-name="dom"]'),
        ALL_FILTERS: getElems('.flt-btn[data-sect-name="dom"]'),
        DDR_FILTER: null,
        GB_FILTER: getElems('.flt-gb[data-sect-name="dom"]')
    };
    const sata = {
        CARDS: getElems('.pr-c[data-sect-name="sata"]'),
        ALL_FILTERS: getElems('.sata-btn'),
        DDR_FILTER: getElems('.sata-btn'),
        GB_FILTER: null,
        sata: true
    };

    const findhaventFilter = ({ CARDS, ALL_FILTERS, DDR_FILTER, GB_FILTER, sata }) => {
        if(!CARDS) return
        function diff(a, b) {
            return Array.prototype.slice.call(a, 0).filter(function (i) { return b.indexOf(i) < 0; });
        };
        
        let trashDdr = [],
            trashGb = [];
        if (DDR_FILTER) {
            for (let i = 0; i < DDR_FILTER.length; i++) {
                let btnDdr = sata ? DDR_FILTER[i][dataset].x : DDR_FILTER[i][dataset].ddr;
                for (let x = 0; x < CARDS.length; x++) {
                    // if(CARDS[x] )
                    // console.log(btnDdr)
                    let cardDdr = CARDS[x][dataset].ddr;
        
                    if (btnDdr == cardDdr) {
                        trashDdr.push(DDR_FILTER[i])
                    }
                }
            }
        }
        if (GB_FILTER) {
            for (let i = 0; i < GB_FILTER.length; i++) {
                let btnDdr = GB_FILTER[i][dataset].gb;
        
                for (let x = 0; x < CARDS.length; x++) {
                    let cardDdr = CARDS[x][dataset].gb;
        
                    if (btnDdr == cardDdr) {
                        trashGb.push(GB_FILTER[i])
                    }
                }
            }
        }

        const uniqueArrayBtns = [...$.unique(trashDdr), ...$.unique(trashGb)];

        const result = diff(ALL_FILTERS, uniqueArrayBtns)
        // console.log(result, 'result')
        for(let res of result) {
            res.setAttribute('disabled', 'disabled');
        }

    };
    findhaventFilter(dimm);
    findhaventFilter(sodimm);
    findhaventFilter(dom);
    findhaventFilter(sata);
})(document);